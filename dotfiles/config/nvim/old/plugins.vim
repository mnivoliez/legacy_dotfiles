"# vim: tabstop=2 shiftwidth=2 expandtab

" Install Vim Plug if not installed
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim                                                             
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC 
endif                       

function! DoRemote(arg)
  UpdateRemotePlugins
endfunction

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.local/share/nvim/plugged')

" looking
  Plug 'airblade/vim-gitgutter'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'morhetz/gruvbox'
  Plug 'felixhummel/setcolors.vim'
" completion/templating
  "Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
  "Plug 'Shougo/neosnippet.vim'
  "Plug 'Shougo/neosnippet-snippets'
  Plug 'SirVer/ultisnips'
  Plug 'honza/vim-snippets'
  Plug 'jiangmiao/auto-pairs'
  Plug 'scrooloose/nerdcommenter'
" command extention
  Plug 'easymotion/vim-easymotion'
  Plug 'tpope/vim-surround'
  Plug 'junegunn/vim-easy-align'
  Plug 'terryma/vim-multiple-cursors'
" utils
  Plug 'neomake/neomake'
  Plug 'sakhnik/nvim-gdb', { 'do': './install.sh' }
  Plug 'editorconfig/editorconfig-vim'
" misc
  Plug 'w0rp/ale'
" documentation
" navigation
  Plug 'Shougo/denite.nvim'
  Plug 'chemzqm/denite-extra'
  Plug 'Shougo/echodoc'
  Plug 'scrooloose/nerdtree'
  Plug 'majutsushi/tagbar'
  "Plug 'xolox/vim-easytags'
  Plug 'ludovicchabant/vim-gutentags'
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
  Plug 'junegunn/fzf.vim'
" vcs
  Plug 'tpope/vim-fugitive'
" language server
  "Plug 'autozimu/LanguageClient-neovim', {
    "\ 'branch': 'next',
    "\ 'do': 'bash install.sh',
    "\ }
  Plug 'neoclide/coc.nvim', {'tag': '*', 'do': { -> coc#util#install()}}
" c/c++
  Plug 'vim-scripts/a.vim'
" rust
  Plug 'rust-lang/rust.vim'

" Initialize plugin system
call plug#end()

" Echodoc conf
" TODO: move that part elsewhere.
set cmdheight=2
let g:echodoc#enable_at_startup = 1
let g:echodoc#type = 'signature'

" wOrp/Ale
let g:ale_sign_column_always = 1
