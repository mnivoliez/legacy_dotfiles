" Use deoplete.
let g:deoplete#enable_at_startup = 1
" disable autocomplete by default
"let b:deoplete_disable_auto_complete=1
"let g:deoplete_disable_auto_complete=1
"call deoplete#custom#buffer_option('auto_complete', v:false)

if !exists('g:deoplete#omni#input_patterns')
 let g:deoplete#omni#input_patterns = {}
endif

let g:deoplete#sources = {}

call deoplete#custom#source('_', 'diasbled_syntaxes', ['Comment', 'String'])

call deoplete#custom#option('sources', {
    \ 'rust': ['LanguageClient'],
    \ 'cpp': ['LanguageClient'],
\})


" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif
