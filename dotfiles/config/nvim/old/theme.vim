set termguicolors
"let g:rehash256 = 1
set background=dark
"colorscheme solarized 
"let g:molokai_original = 1
colorscheme gruvbox

let g:airline_theme='base16_gruvbox'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#gutentags#enabled = 1

" Always draw the signcolumn.
set signcolumn=yes
