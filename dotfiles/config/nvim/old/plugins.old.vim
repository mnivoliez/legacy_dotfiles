"# vim: tabstop=2 shiftwidth=2 expandtab

"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('~/.cache/dein')
  call dein#begin('~/.cache/dein')

  " Let dein manage dein
  " Required:
  call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')

  " Add or remove your plugins here:
  call dein#add('neomake/neomake')
  call dein#add('Shougo/deoplete.nvim')
  call dein#add('Shougo/denite.nvim')
  call dein#add('Shougo/neosnippet.vim')
  call dein#add('Shougo/neosnippet-snippets')
  call dein#add('Shougo/echodoc')
  call dein#add('autozimu/LanguageClient-neovim')
  call dein#add('rust-lang/rust.vim')
  call dein#add('mattn/emmet-vim')
  call dein#add('sakhnik/nvim-gdb')
  call dein#add('OmniSharp/omnisharp-vim')
  call dein#add('tikhomirov/vim-glsl')
  call dein#add('mhartington/nvim-typescript', {'build': './install.sh'})
  call dein#add('leafgarland/typescript-vim')
  call dein#add('mattn/emmet-vim')
  call dein#add('ternjs/tern_for_vim', { 'do': 'npm install && npm install -g tern' } )

  "call dein#add('HerringtonDarkholme/yats.vim')

  call dein#add('tpope/vim-fugitive')
  call dein#add('airblade/vim-gitgutter')
  call dein#add('tpope/vim-surround')

  call dein#add('godlygeek/tabular')
  
  call dein#add('jiangmiao/auto-pairs')

  call dein#add('felixhummel/setcolors.vim')
  call dein#add('flazz/vim-colorschemes')
  call dein#add('iCyMind/NeoSolarized')

  call dein#add('scrooloose/nerdtree')
  call dein#add('scrooloose/nerdcommenter')

  call dein#add('vim-airline/vim-airline')
  call dein#add('vim-airline/vim-airline-themes')

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif
