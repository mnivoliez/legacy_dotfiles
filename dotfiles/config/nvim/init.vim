"# vim: tabstop=2 shiftwidth=2 expandtab

" vimrc files
for s:path in split(glob('~/.config/nvim/vimrc/*.vim'), "\n")
  exe 'source ' . s:path
endfor

"source $HOME/.config/nvim/plugins.vim
"source $HOME/.config/nvim/theme.vim
"source $HOME/.config/nvim/deoplete.vim
"source $HOME/.config/nvim/language_client.vim
"source $HOME/.config/nvim/keybinding.vim

"Correct filetype----------------------------
autocmd BufReadPost *.rs setlocal filetype=rust
autocmd BufReadPost *.html setlocal filetype=html

" General---------------------------------
"End general------------------------------


"color theme------------------------------
"End color theme--------------------------
