" vim: set sw=2 ts=2 sts=2 et tw=78 foldmarker={{,}} foldmethod=marker foldlevel=0:
" General option {{
  set termguicolors
  set hidden
  set signcolumn=yes
  " Formating
  set expandtab
  set shiftwidth=2
  set softtabstop=2
  set autoindent
  set number
  set grepprg=rg\ --vimgrep\ $*
  set grepformat=%f:%l:%c:%m
  set title

  " Smaller updatetime for CursorHold & CursorHoldI
  set updatetime=300
" }}

" Theme {{
  set background=dark
  colorscheme gruvbox
  highlight Normal guibg=none
  highlight NonText guibg=none

  let g:airline_theme='gruvbox'
  let g:airline_powerline_fonts = 1
  let g:airline#extensions#tabline#enabled = 1
  let g:airline#extensions#gutentags#enabled = 1


	let g:airline_section_error = '%{airline#util#wrap(airline#extensions#coc#get_error(),0)}'
	let g:airline_section_warning = '%{airline#util#wrap(airline#extensions#coc#get_warning(),0)}'
" }}

" Syntax related {{
  " improve performance
  "syntax sync minlines=300
  hi Pmenu  guifg=#111111 guibg=#F8F8F8 ctermfg=black ctermbg=Lightgray
  hi PmenuSbar  guifg=#8A95A7 guibg=#F8F8F8 gui=NONE ctermfg=darkcyan ctermbg=lightgray cterm=NONE
  hi PmenuThumb  guifg=#F8F8F8 guibg=#8A95A7 gui=NONE ctermfg=lightgray ctermbg=darkcyan cterm=NONE
  " change default search highlight
  hi Search guibg=#111111 guifg=#C5B569
  if !has('gui_running') | hi normal guibg=NONE | endif
  call matchadd('ColorColumn', '\%81v', 100)
  hi ColorColumn ctermbg=magenta ctermfg=0 guibg=#333333
  hi HighlightedyankRegion term=bold ctermbg=0 guibg=#13354A

  highlight link CocErrorSign   GruvboxRedSign
  highlight link CocWarningSign GruvboxYellowSign
  highlight link CocInfoSign    GruvboxYellowSign
  highlight link CocHintSign    GruvboxBlueSign

  " cpp syntax
  let g:cpp_class_scope_highlight = 1
  let g:cpp_member_variable_highlight = 1
  let g:cpp_class_decl_highlight = 1
  let g:cpp_experimental_simple_template_highlight = 1
  let g:cpp_concepts_highlight = 1
" }}

" Complete config {{
  "set complete+=k
  "set complete-=t
  ""set completeopt=menu,preview
  "set completeopt=menuone,noinsert

  "if !has('nvim')
    ""set balloonevalterm
    ""set ballooneval
    "" cursor shape of vim
    ""let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    ""let &t_SR = "\<Esc>]50;CursorShape=2\x7"
    ""let &t_EI = "\<Esc>]50;CursorShape=0\x7"
    "" make <M-s> for saving
    "execute "set <M-s>=\es"
    "execute "set <M-c>=\ec"
  "else
    "set fillchars+=msgsep:-
    "highlight link MsgSeparator MoreMsg
  "endif
" }}
