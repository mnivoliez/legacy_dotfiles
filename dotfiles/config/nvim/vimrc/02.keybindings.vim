" vim: set sw=2 ts=2 sts=2 et tw=78 foldmarker={{,}} foldmethod=marker foldlevel=0:
let mapleader = "\<SPACE>"

" basic {{
  " show vim highlight group under cursor
  nnoremap  <leader>hi :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
    \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
    \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

  nnoremap <silent> <leader>o :call <SID>Open()<CR>
" }}

" plugins {{
  " denite
  "
  " denite-extra session helper
  nmap <leader>ss :call <SID>SessionSave()<CR>
  nmap <leader>sl :<C-u>SessionLoad 
  nmap <leader>sr :call <SID>SessionReload()<CR>

  " coc.nvim
  nmap <leader>rn <Plug>(coc-rename)
  vmap <leader>f  <Plug>(coc-format-selected)
  nmap <leader>f  <Plug>(coc-format-selected)
  vmap <leader>a  <Plug>(coc-codeaction-selected)
  nmap <leader>a  <Plug>(coc-codeaction-selected)
  nmap <leader>ac <Plug>(coc-codeaction)

  " Remap keys for gotos
  nmap <silent> gd <Plug>(coc-definition)
  nmap <silent> gy <Plug>(coc-type-definition)
  nmap <silent> gi <Plug>(coc-implementation)
  nmap <silent> gr <Plug>(coc-references)

  " Remap for formating
  nnoremap <silent> L :Format<CR> 

  " Use K for show documentation in preview window
  nnoremap <silent> K :call <SID>show_documentation()<CR>
  
  function! s:show_documentation()
    if &filetype == 'vim'
      execute 'h '.expand('<cword>')
    else
      call CocAction('doHover')
    endif
  endfunction

  " use <tab> for trigger completion and navigate to next complete item
  function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
  endfunction

  inoremap <silent><expr> <TAB>
        \ pumvisible() ? "\<C-n>" :
        \ <SID>check_back_space() ? "\<TAB>" :
        \ coc#refresh()
  " Use <c-space> for trigger completion.
  inoremap <silent><expr> <c-space> coc#refresh()

  inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
  inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
  
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
  inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>"
  autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

  " Use <C-l> for trigger snippet expand.
  imap <C-l> <Plug>(coc-snippets-expand)

  " Use <C-j> for select text for visual placeholder of snippet.
  vmap <C-j> <Plug>(coc-snippets-select)

  " Use <C-j> for jump to next placeholder, it's default of coc.nvim
  let g:coc_snippet_next = '<c-j>'
  
  " Use <C-k> for jump to previous placeholder, it's default of coc.nvim
  let g:coc_snippet_prev = '<c-k>'

  " Use <C-j> for both expand and jump (make expand higher priority.)
  imap <C-j> <Plug>(coc-snippets-expand-jump)

  inoremap <silent><expr> <TAB>
      \ pumvisible() ? coc#_select_confirm() :
      \ coc#expandableOrJumpable() ? coc#rpc#request('doKeymap', ['snippets-expand-jump','']) :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

  let g:coc_snippet_next = '<tab>'

  
  " Shortcuts for denite interface
  " Show extension list
  nnoremap <silent> <leader>dce  :<C-u>Denite coc-extension<cr>
  " Show symbols of current buffer
  nnoremap <silent> <leader>dcs  :<C-u>Denite coc-symbols<cr>
  " Search symbols of current workspace
  nnoremap <silent> <leader>dcw  :<C-u>Denite coc-workspace<cr>
  " Show diagnostics of current workspace
  nnoremap <silent> <leader>dcd :<C-u>Denite coc-diagnostic<cr>
  " Show available commands
  nnoremap <silent> <leader>dcc  :<C-u>Denite coc-command<cr>
  " Show available services
  nnoremap <silent> <leader>dcs  :<C-u>Denite coc-service<cr>
  " Show links of current buffer
  nnoremap <silent> <leader>dcl  :<C-u>Denite coc-link<cr>

  " EasyAlign
  xmap ga <Plug>(LiveEasyAlign)
  nmap ga <Plug>(LiveEasyAlign)

  " gitgutter
  nmap [g <Plug>GitGutterPrevHunk
  nmap ]g <Plug>GitGutterNextHunk

  " ale
  nmap <silent> [a <Plug>(ale_previous_wrap)
  nmap <silent> ]a <Plug>(ale_next_wrap)

  " NERDTree
  nmap <F4> :NERDTreeToggle<CR>

  " TagBar
  nmap <F5> :TagbarToggle<CR>

  " FZF
  nmap <silent> <leader>fzf :<C-u>FZF<cr>

" }}

" grep by motion {{
  vnoremap <leader>g :<C-u>call <SID>GrepFromSelected(visualmode())<CR>
  nnoremap <leader>g :<C-u>set operatorfunc=<SID>GrepFromSelected<CR>g@

  function! s:GrepFromSelected(type)
    let saved_unnamed_register = @@
    if a:type ==# 'v'
      normal! `<v`>y
    elseif a:type ==# 'char'
      normal! `[v`]y
    else
      return
    endif
    let word = substitute(@@, '\n$', '', 'g')
    let word = escape(word, '| ')
    let @@ = saved_unnamed_register
    execute 'Denite grep:::'.word
  endfunction
" }}

" functions {{
  function! s:SessionSave()
    if !empty(v:this_session)
      execute 'SessionSave'
    else
      call feedkeys(':SessionSave ')
    endif
  endfunction

  function! s:SessionReload()
    execute 'wa'
    execute 'RestartVim'
  endfunction

  function! s:Open()
    let res = CocAction('openLink')
    if res | return | endif
    let line = getline('.')
    " match url
    let url = matchstr(line, '\vhttps?:\/\/[^)\]''" ]+')
    if !empty(url)
      let output = system('open '. url)
    else
      let mail = matchstr(line, '\v([A-Za-z0-9_\.-]+)\@([A-Za-z0-9_\.-]+)\.([a-z\.]+)')
      if !empty(mail)
        let output = system('open mailto:' . mail)
      else
        let output = system('open ' . expand('%:p:h'))
      endif
    endif
    if v:shell_error && output !=# ""
      echoerr output
    endif
  endfunction
" }}
