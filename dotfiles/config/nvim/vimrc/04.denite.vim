"call denite#custom#option('default', 'direction', 'bottom')
call denite#custom#option('default', 'empty', 0)
call denite#custom#option('default', 'auto_resize', 1)
call denite#custom#option('default', 'auto_resume', 1)
call denite#custom#filter('matcher_ignore_globs', 'ignore_globs',
  \ [ '.git/', '.ropeproject/', '__pycache__/', '*.min.*', 'fonts/'])
" Change file_rec command.
call denite#custom#var('file_rec', 'command',
  \ ['rg', '--color', 'never', '--files'])
" buffer
call denite#custom#var('buffer', 'date_format', '')
" Change grep options.
call denite#custom#var('grep', 'command', ['rg'])
call denite#custom#var('grep', 'default_opts', ['--vimgrep', '--no-follow'])
call denite#custom#var('grep', 'recursive_opts', [])
call denite#custom#var('grep', 'pattern_opt', [])
call denite#custom#var('grep', 'separator', ['--'])
call denite#custom#var('grep', 'final_opts', [])
" Change file_rec matcher
call denite#custom#source('_', 'matchers', ['matcher/fuzzy'])
call denite#custom#source('line', 'matchers', ['matcher_regexp'])
call denite#custom#source('redis_mru', 'sorters', [])
